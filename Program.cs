﻿using System;

namespace NameAndCity
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    
         public static string NameAndCity(string name, string city)
        {
            return $"{name} lives in {city}";
        }
    }
}
